//
//  ViewController.swift
//  SiwftSpeechToTextDemo
//
//  Created by 富田篤 on 2020/03/02.
//  Copyright © 2020 Bird Tomita. All rights reserved.
//

import UIKit

let powerThreshold : Float32 = -35.0

class ViewController: UIViewController {
    
    var speechToText : SpeechRecognizer!

    var accumulator = SpeechRecognitionResultsAccumulator()
    var isStreaming = false
            
    var isSpeaking = false
    
    @IBOutlet weak var textView: UILabel!
    @IBOutlet weak var powerLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
                
        speechToText = SpeechRecognizer(apiKey: "KNrViY63XKFymEK1M-9bvtpN5bSj9E3fGCSejjr5wYr8")
    }
    
    @IBAction func onRecButtonTap(_ sender: UIButton) {
        
        if !isStreaming {
            isStreaming = true
            sender.setTitle("Stop Microphone", for: .normal)
            var settings = RecognitionSettings(contentType: "audio/l16;rate=16000;channels=1")
            settings.interimResults = true
            var callback = RecognizeCallback()
            callback.onError = { error in
                print(error)
            }
            callback.onResults = { results in
                self.accumulator.add(results: results)
                print(self.accumulator.bestTranscript)
                self.textView.text = self.accumulator.bestTranscript
            }
            let onPowerData : ((Float32) -> Void) = { power in
                self.powerLabel.text = "Power = \(power)"
                
                if (!self.isSpeaking && power > powerThreshold) {
                    self.isSpeaking = true
                    self.powerLabel.textColor = .red
                    print("start")
                } else if (self.isSpeaking && power <= powerThreshold) {
                    self.isSpeaking = false
                    self.powerLabel.textColor = .black
                    print("stop")
                }
                
            }

            speechToText.recognizeMicrophone(settings: settings, model:"ja-JP_BroadbandModel",onPowerData: onPowerData,callback: callback)
        } else {
            isStreaming = false
            sender.setTitle("Start Microphone", for: .normal)
            speechToText.stopRecognizeMicrophone()
        }
    }
    
}

