//
//  InsecureConnections.swift
//  SiwftSpeechToTextDemo
//
//  Created by 富田篤 on 2020/03/03.
//  Copyright © 2020 Bird Tomita. All rights reserved.
//
import Foundation

class InsecureConnection {
    /// Allow network requests to a server without verification of the server certificate.
    /// **IMPORTANT**: This should ONLY be used if truly intended, as it is unsafe otherwise.
    static func session() -> URLSession {
        return URLSession(configuration: .default, delegate: AllowInsecureConnectionDelegate(), delegateQueue: nil)
    }
}

/**
 URLSession delegate that is used to bypass SSL certificate verification.

 **IMPORTANT**: This can potentially cause dangerous security breaches, so use only if you are certain that you have taken necessary precautions.
 */

class AllowInsecureConnectionDelegate: NSObject, URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
    }
}
