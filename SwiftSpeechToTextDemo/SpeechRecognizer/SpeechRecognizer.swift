//
//  SpeechRecognizer.swift
//  SiwftSpeechToTextDemo
//
//  Created by 富田篤 on 2020/03/03.
//  Copyright © 2020 Bird Tomita. All rights reserved.
//

import UIKit
import RestKit

class SpeechRecognizer {
    public var serviceURL = "https://stream.watsonplatform.net/speech-to-text/api"
    internal let serviceName = "SpeechToText"
    internal let serviceVersion = "v1"
    public var defaultHeaders = [String: String]()
    let userAgent = "watson-apis-swift-sdk/2.3.0/iOS/13.1.0"

    var session = URLSession(configuration: URLSessionConfiguration.default)
    var authMethod: AuthenticationMethod
    
    public init(apiKey: String, iamUrl: String? = nil) {
        RestRequest.userAgent = self.userAgent
        self.authMethod = IAMAuthentication(apiKey: apiKey, url: iamUrl)
    }
//
//    public func disableSSLVerification() {
//        session = InsecureConnection.session()
//    }

}
